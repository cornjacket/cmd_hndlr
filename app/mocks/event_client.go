package mocks

import (
	"log"

	client "bitbucket.org/cornjacket/event_hndlr/client_lib"
	"bitbucket.org/cornjacket/iot/message"
	"bitbucket.org/cornjacket/iot/serverlib/tallocrespproc"
)

// Should I rename this clientMock
type EventHndlrServiceMock struct {
}

var EventLastRxPacket message.UpPacket
var EventLastTallocResp tallocrespproc.TallocResp

//func (s *EventHndlrServiceMock) Open(Hostname, Port, PacketPath, TallocRespPath string) error {
func (s *EventHndlrServiceMock) Open(config client.EventHndlrServiceConfig) error {
	return nil
}

func (s *EventHndlrServiceMock) SendPacket(packet message.UpPacket) error {
	log.Println("EventClientMock.SendPacket() invoked.")
	EventLastRxPacket = packet
	return nil
}

func (s *EventHndlrServiceMock) SendTallocResp(tallocresp tallocrespproc.TallocResp) error {
	log.Println("EventClientMock.SendTallocResp() invoked.")
	EventLastTallocResp = tallocresp
	return nil
}
