package controllers

import (
	// Standard library packets
	"fmt"
	"net/http"

	// Third party packages
	"bitbucket.org/cornjacket/iot/loriot"
	"bitbucket.org/cornjacket/iot/message"
	"bitbucket.org/cornjacket/iot/serverlib/pktproc2"
	"github.com/julienschmidt/httprouter"
)

// A message.UpPacket is returned in the response
// swagger:response packetResponse
type packetResponseWrapper struct {
	// Packet send to Command Handler
	// in: body
	Body message.UpPacket
}

// swagger:route POST /packet packet sendPacket
// Sends a LoRa packet either to the Event Handler (rx) or to AirTrafficControl (gw) via the Command Handler.
// Returns LoRa packet to client with correlation_id
// responses:
//   201: packetResponse

// This function returns the entry point for the packet processing worker pool upon reception of
// a post to /packet. The default behavior is for the pktproc2 post worker to immediately queue the packet
// followed by replying to the client with the posted packet.
func (app *AppContext) NewParserPoolEntryFunc(class_id string, numWorkers int) func(http.ResponseWriter, *http.Request, httprouter.Params) {
	return pktproc2.NewPacketIngestionHandlerFunc(class_id, numWorkers, app.postPacketHandler)

}

// postPacketHandler accepts a LoRa packet and forwards to ATC or the EventHandler.
// This function serves as the work process executed by the pktproc2 worker pool.
// This func returns true in all cases. Not so nice.
// TODO: Instead of returning bool, func should return err so caller can react
// TODO: On error upper layer logic should perform a backoff or alternate action instead of continually sending to a down service
func (app *AppContext) postPacketHandler(wID int, packet message.UpPacket) bool {

	// TODO: Should validate ts where packet is too stale. ie. problem with the network server. This packet should have a special destination
	if loriot.IsRxPacket(packet) {
		err := app.EventHndlrService.SendPacket(packet)
		pktproc2.DisplayPassFailMessageWithLatency("Send Rx packet to App Event Handler", err, wID, &packet, true)
	} else if loriot.IsGwPacket(packet) {
		err := app.AirTrafficControlService.SendPacket(packet)
		pktproc2.DisplayPassFailMessageWithLatency("Send Gw packet to AirTrafficControl", err, wID, &packet, true)
	} else if loriot.IsTxPacket(packet) {
		fmt.Printf("FAIL, worker %d: Not forwarding tx packet.\n", wID)
	} else {
		fmt.Printf("FAIL, worker %d: Not forwarding unknown packet. cmd=: %s\n", wID, packet.Cmd)
	}

	return true
}
