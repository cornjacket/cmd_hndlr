package app

import (
	"fmt"
	"log"
	"testing"
	"time"

	. "gopkg.in/check.v1"

	"bitbucket.org/cornjacket/cmd_hndlr/app/controllers"
	"bitbucket.org/cornjacket/cmd_hndlr/app/mocks"
	cmd_client "bitbucket.org/cornjacket/cmd_hndlr/client_lib"
	"bitbucket.org/cornjacket/iot/message"
	"bitbucket.org/cornjacket/iot/serverlib/tallocrespproc"
)

var pktClient cmd_client.PacketCmdHndlrService
var tRespClient cmd_client.TallocRespCmdHndlrService

func NewAppServiceMock() AppService {
	a := AppService{}
	a.Context = NewAppContextMock()
	return a
}

func NewAppContextMock() *controllers.AppContext {
	context := controllers.AppContext{}
	context.EventHndlrService = &mocks.EventHndlrServiceMock{}               // implements EventHandlrPostInterface
	context.AirTrafficControlService = &mocks.AirTrafficControlServiceMock{} // implements AirTrafficControlPostInterface
	return &context
}

// Hook up gocheck into the "go test" runner.
func Test(t *testing.T) { TestingT(t) }

type TestSuite struct {
	AppService AppService
}

var _ = Suite(&TestSuite{})

func (s *TestSuite) SetUpSuite(c *C) {
	fmt.Println("SetUpSuite() invoked")
	s.AppService = NewAppServiceMock()
	s.AppService.InitEnv()
	go s.AppService.Run()
	// time delay to guarantee so that the server is up and talking to the database
	time.Sleep(1 * time.Second)
	// Initialize Packet Client
	if err := pktClient.Open("localhost", s.AppService.Env.PortNum, "/packet"); err != nil {
		//log.Fatal("pktClient.Open() error: %s", err)
		log.Fatal("pktClient.Open() failed")
	}
	// Initialize TallocResp Client
	if err := tRespClient.Open("localhost", s.AppService.Env.PortNum, "/tallocresp"); err != nil {
		//log.Fatal("tRespClient.Open() error: %s", err)
		log.Fatal("tRespClient.Open() failed")
	}

}

func (s *TestSuite) SetUpTest(c *C) {
	fmt.Println("SetupTest() invoked")
}

func (s *TestSuite) TearDownTest(c *C) {
	fmt.Println("TearDownTest() invoked")
}

func (s *TestSuite) TearDownSuite(c *C) {
	fmt.Println("TearDownSuite() invoked")
	// TODO(drt) - close the db connection
}

func (s *TestSuite) TestRxPacketPath(c *C) {
	// Send Packet
	fmt.Printf("component_test.packetTest\n")

	p := message.UpPacket{
		Dr:   "?",
		Ts:   uint64(12345),
		Eui:  "1234",
		Ack:  false,
		Cmd:  "rx",
		Snr:  12.4,
		Data: "12345678", // this is a random packet that should get rejected by the controller
		Fcnt: 12,
		Freq: uint64(20),
		Port: 1,
		Rssi: -110,
	}
	err := pktClient.SendPacket(p)
	c.Assert(err, Equals, nil)
	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the event_hndlr is finished processing

	c.Assert(mocks.EventLastRxPacket.Data, Equals, p.Data)
	c.Assert(mocks.EventLastRxPacket.Eui, Equals, p.Eui)
	c.Assert(mocks.EventLastRxPacket.Cmd, Equals, p.Cmd)

}

func (s *TestSuite) TestTallocRespPath(c *C) {
	// Send Packet
	fmt.Printf("component_test.tallocrespTest\n")

	r := tallocrespproc.TallocResp{
		NodeEui: "1234",
		Tslot:   4,
		Code:    1, // >0 will be valid, otherwise error code
		Test:    false,
		Ts:      uint64(0),
		CorrId:  uint64(1),
		ClassId: "4.3.6",
		Hops:    1,
	}

	err := tRespClient.SendTallocResp(r)
	c.Assert(err, Equals, nil)
	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the event_hndlr is finished processing

	c.Assert(mocks.EventLastTallocResp.NodeEui, Equals, r.NodeEui)
	c.Assert(mocks.EventLastTallocResp.Tslot, Equals, r.Tslot)
	c.Assert(mocks.EventLastTallocResp.Code, Equals, r.Code)

}

func (s *TestSuite) TestGwPacketPath(c *C) {
	// Send Packet
	fmt.Printf("component_test.packetTest\n")

	p := message.UpPacket{
		Dr:   "?",
		Ts:   uint64(12345),
		Eui:  "1234",
		Ack:  false,
		Cmd:  "gw",
		Snr:  12.4,
		Data: "12345678", // this is a random packet that should get rejected by the controller
		Fcnt: 12,
		Freq: uint64(20),
		Port: 1,
		Rssi: -110,
	}
	err := pktClient.SendPacket(p)
	c.Assert(err, Equals, nil)
	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the event_hndlr is finished processing

	c.Assert(mocks.AtcLastPacket.Data, Equals, p.Data)
	c.Assert(mocks.AtcLastPacket.Eui, Equals, p.Eui)
	c.Assert(mocks.AtcLastPacket.Cmd, Equals, p.Cmd)

}
